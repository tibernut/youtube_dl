FROM python:3-slim
MAINTAINER Chris Jones <chris_s_jones@outlook.com>

RUN apt update -y && apt install -y git && git clone https://gitlab.com/tibernut/youtube_dl.git && pip3 install feedparser && pip3 install PyMySQL && pip3 install pytube && pip3 install python-dateutil


CMD ["python","/youtube_dl/POC.py"]

